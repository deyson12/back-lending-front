from flask import Flask, escape, request, jsonify
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

class HelloWorld(Resource):
    def get(self):
        name = request.args.get("name", "World")
        message = f'Hello, {escape(name)}!'
        return {'about': message}
        
    def post(self):
        some_josn = request.get_json()
        return {'you send': some_josn}, 201
        
class Multi(Resource):
    def get(self, num):
        return {'result': num*10}
        
api.add_resource(HelloWorld, '/hello')
api.add_resource(Multi, '/multi/<int:num>')