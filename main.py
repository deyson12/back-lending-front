from flask import Flask, escape, request, jsonify

app = Flask(__name__)

@app.route('/')
def hello():
    name = request.args.get("name", "World")
    message = f'Hello, {escape(name)}!'
    return jsonify({"name": escape(name), "message": message})
    
@app.route('/hola')
def hello():
    name = request.args.get("name", "World")
    message = f'Hola, {escape(name)}!'
    return jsonify({"name": escape(name), "message": message})